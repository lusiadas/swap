# Swap
> A plugin for [Oh My Fish](https://www.github.com/oh-my-fish/oh-my-fish).

[![GPL License](https://img.shields.io/badge/license-GPL-blue.svg?longCache=true&style=flat-square)](/LICENSE)
[![Fish Shell Version](https://img.shields.io/badge/fish-v2.7.1-blue.svg?style=flat-square)](https://fishshell.com)
[![Oh My Fish Framework](https://img.shields.io/badge/Oh%20My%20Fish-Framework-blue.svg?style=flat-square)](https://www.github.com/oh-my-fish/oh-my-fish)

<br/>

## Description

A wrapper function to allow `mv` to swap the location or contents of targeted files or folders

## Syntax
```
mv -s [-c] [file] [file]
```

## Options
```
-s/--swap
Trigger this function

-c/--contents
Swap contents intead of location. Contents of a file can only be swaped with those of another file, the same applies for folders.

-h/--help
Display the function description and instructions
```

## Install

Either with omf
```fish
omf install swap
```
or [fisherman](https://github.com/fisherman/fisherman)
```fish
fisher gitlab.com/lusiadas/swap
```

---

Ⓐ Made in Anarchy. No wage slaves were economically coerced into the making of this work.
