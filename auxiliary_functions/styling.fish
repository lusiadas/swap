set -l reg (set_color normal); set -l bld (set_color --bold)
function win -V reg
  set -l creg (set_color normal)(set_color green)
  set -l cbld (set_color green --bold)
  set -l bold false
  while string match -qe '|' "$argv"
    if test $bold = false
      set argv (string replace '|' "$cbld" "$argv"); set bold true
    else
      set argv (string replace '|' "$creg" "$argv"); set bold false
    end
  end
  echo $creg"✔ $argv"$reg
end
function wrn -V reg
  set -l creg (set_color normal)(set_color yellow)
  set -l cbld (set_color yellow --bold)
  set -l bold false
  while string match -qe '|' "$argv"
    if test $bold = false
      set argv (string replace '|' "$cbld" "$argv"); set bold true
    else
      set argv (string replace '|' "$creg" "$argv"); set bold false
    end
  end
  echo $cbld"! "$creg"$argv"$reg
end
function err -V reg
  set -l creg (set_color normal)(set_color red)
  set -l cbld (set_color red --bold)
  set -l bold false
  while string match -qe '|' "$argv"
    if test $bold = false
      set argv (string replace '|' "$cbld" "$argv"); set bold true
    else
      set argv (string replace '|' "$creg" "$argv"); set bold false
    end
  end
  echo $creg"✘ $argv"$reg
end
function dim -V reg
  set -l creg (set_color normal)(set_color -d)
  set -l cbld (set_color -od)
  set -l bold false
  while string match -qe '|' "$argv"
    if test $bold = false
      set argv (string replace '|' "$cbld" "$argv"); set bold true
    else
      set argv (string replace '|' "$creg" "$argv"); set bold false
    end
  end
  echo -- $creg"$argv"$reg
end
