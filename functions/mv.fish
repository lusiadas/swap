function mv -d "Swap the location or contents between two files or folders"
  status -f | string match -r '.*(?=(/[^/]+){2}$)' | read -l root
  for file in (find $root/auxiliary_functions/*); source "$file"; end
  set -l bld (set_color --bold); set -l reg (set_color normal)
  set -l info $bld"
DESCRIPTION"$reg"
Swap the location or contents between two files or folders

"$bld"SYNTAX
mv"$reg" [-s/-c] [file] [file]

"$bld"OPTIONS"$reg"
-s/ --swap
Trigger this function

-c/--swap-contents
Swap the contents intead of files or folders

-h
Display these instructions
"
  switch $argv[1]

  case -s --swap

    # Check argument validity
    if test (count $argv) -ne 3
      echo -e $info | grep -A 1 -e "SYNTAX"; return 1
    end
    set -l fail
    for arg in $argv[2 3]
      test -e $arg; and continue
      err "|$arg| not found"; set fail true
    end
    test -z "$fail"; or return 1

    # Swap locations
    if string match -qv (dirname $argv[2]) (dirname $argv[3])
      mktemp -d | read -l tmp
      mv $argv[2] $tmp
      mv $argv[3] (dirname $argv[2]); mv $tmp/* (dirname $argv[3])
      win "The location of |$argv[2]| and |$argv[3]| have been swapped"
      return 1
    end
    wrn "Targets are in the same location."
    read -lP "Swap their contents instead? [y/n]: " opt
    string match -qr -- '(?i)^y$' $opt; or return 1
    eval "mv -sc $argv"

  case -c --swap-contents

    # Check argument validity
    if test (count $argv) -ne 3
      echo $instructions | grep -A 1 -e "SYNTAX"; return 1
    end
    set -l type; set -l fail
    for arg in $argv
      if not test -e $arg
        err "|$arg| not found"; set fail true; continue
      else if test -f $arg
        test -n "$type"; or set type file
        string match -q $type file; and continue
      end
      test -n "$type"; or set type folder
      string match -q $type folder; and continue
      err "Cannot swap the contents of a file for those of a folder"; return 1
    end
    test -z "$fail"; or return 1

    # Swap contents
    if string match -q $type folder
      mktemp -d | read -l tmp
      mv  $args[2]/* $tmp; mv  $argv[3]/* $argv[2]; mv  $tmp/* $argv[3]
    else
      mktemp | read -l tmp
      mv $argv[2] $tmp; mv $argv[3] $argv[2]; mv $tmp $argv[3]
    end
    win "The contents of |$argv[2]| and |$argv[3]| have been swapped"

  case -h; echo $info; test (count $argv) -eq 1

  case '*' ''; command mv $argv
  end
end
