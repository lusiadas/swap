source /usr/share/fish/completions/mv.fish
complete -c mv -s s -l swap -d 'Swap the location or contents between two files or folders'
complete -c mv -s c -l contents -d 'Swap the location or contents between two files or folders'
